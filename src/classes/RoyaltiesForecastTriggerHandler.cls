public class RoyaltiesForecastTriggerHandler extends TriggerHandler {

  public override void beforeInsert() {
    RoyaltyCalculationHandler.calculateRoyaltyRate(Trigger.new);
  }

}