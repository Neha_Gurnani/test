/**
 * 
 * @description
 *
 * @author BlackTab Solutions
 * @date 1/11/2019
 *
 */
public class RoyaltyCalculationHandler {


  public static void calculateRoyaltyRate(List<Royalty_Forecast__c> newRoyaltyForecasts) {
    if (newRoyaltyForecasts == null || newRoyaltyForecasts.size() == 0) return;

    Set<Id> oppIds = new Set<Id>();
    Map<Id, List<Royalty_Forecast__c>> oppIdToRoyaltyForecastList = new Map<Id, List<Royalty_Forecast__c>>();
    for (Royalty_Forecast__c rfItem : newRoyaltyForecasts) {
      oppIds.add(rfItem.Opportunity__c);
      if (!oppIdToRoyaltyForecastList.containsKey(rfItem.Opportunity__c)) {
        oppIdToRoyaltyForecastList.put(rfItem.Opportunity__c, new List<Royalty_Forecast__c>{rfItem});
      } else {
        oppIdToRoyaltyForecastList.get(rfItem.Opportunity__c).add(rfItem);
      }
    }
    // when a new Royalty Forecast item is insert
    // retrieve all other items already inserted for the Opportunity
    // to calculate volumes and rates properly
    // when RF items are created through Visual Flow they all are inserted in separate transaction
    // so no list of items but actually one by one
    for (Royalty_Forecast__c rfItem : [
      SELECT Forecast_Volume__c, Opportunity__c, Forecast_Year__c
      FROM Royalty_Forecast__c WHERE Opportunity__c IN :oppIds
    ]) {
      oppIdToRoyaltyForecastList.get(rfItem.Opportunity__c).add(rfItem);
    }

    Map<Id, List<Royalty_Quote__c>> oppIdToRoyaltyQuoteList = new Map<Id, List<Royalty_Quote__c>>();
    Map<Id, Double> oppIdToNumberOfCoresMultiplier = new Map<Id, Double>();
    for (Royalty_Quote__c rqItem : [
      SELECT Multiplier__c, N_of_Instantiations__c, Royalty_Percent__c, Volume_Max__c, Volume_Min__c, Opportunity__c,
        Fixed_Royalties_Amount__c, Royalties_Type__c
      FROM Royalty_Quote__c WHERE Opportunity__c IN :oppIds
      ORDER BY Volume_Min__c ASC
    ]) {
      if (!oppIdToRoyaltyQuoteList.containsKey(rqItem.Opportunity__c)) {
        oppIdToRoyaltyQuoteList.put(rqItem.Opportunity__c, new List<Royalty_Quote__c>{rqItem});
      } else {
        oppIdToRoyaltyQuoteList.get(rqItem.Opportunity__c).add(rqItem);
      }
      if (!oppIdToNumberOfCoresMultiplier.containsKey(rqItem.Opportunity__c)) {
        oppIdToNumberOfCoresMultiplier.put(rqItem.Opportunity__c, getNoOfCoresMultiplierForRF(rqItem));
      }
    }
    System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 1 --- \n'
      + '\n - newRoyaltyForecasts: ' + newRoyaltyForecasts
      + '\n - oppIds: ' + oppIds
      + '\n - oppIdToRoyaltyForecastList: ' + oppIdToRoyaltyForecastList
      + '\n - oppIdToRoyaltyQuoteList: ' + oppIdToRoyaltyQuoteList
      + '\n - oppIdToNumberOfCoresMultiplier: ' + oppIdToNumberOfCoresMultiplier
      + '\n');
    for (Id oppIdItem : oppIds) {
      // calculate Royalty rate and forecast
      List<Royalty_Forecast__c> sortedRoyaltyForecasts = getSortedRoyaltyForecastList(oppIdToRoyaltyForecastList.get(oppIdItem));
      List<Royalty_Quote__c> royaltyQuoteList = oppIdToRoyaltyQuoteList.get(oppIdItem);
      if (royaltyQuoteList.size() > 0 && royaltyQuoteList[0].Royalties_Type__c == 'Percentage') {
        Decimal coreMultiplier = oppIdToNumberOfCoresMultiplier.get(oppIdItem);
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 2 --- \n'
          + '\n - oppIdItem: ' + oppIdItem
          + '\n - sortedRoyaltyForecasts: ' + sortedRoyaltyForecasts
          + '\n - royaltyQuoteList: ' + royaltyQuoteList
          + '\n - oppIdToNumberOfCoresMultiplier: ' + oppIdToNumberOfCoresMultiplier
          + '\n - coreMultiplier: ' + coreMultiplier
          + '\n');
        Integer i = 0, j = 0;
        Integer currentRoyaltyQuoteIndex = 0;
        Decimal totalRoyaltyForecastVolume = 0, currentYearRoyaltyForecastVolume, currentYearRoyaltyForecastAmount;
        Decimal currentRoyaltyPercent = royaltyQuoteList[currentRoyaltyQuoteIndex].Royalty_Percent__c / 100;
        //for (Integer i = 0; i < sortedRoyaltyForecasts.size(); i++) {
        Decimal rfVolumeToCalculateWRQTier = 0;
        for (Royalty_Forecast__c rfItem : sortedRoyaltyForecasts) {
          currentYearRoyaltyForecastVolume = 0;
          currentYearRoyaltyForecastAmount = 0;
          rfVolumeToCalculateWRQTier = rfItem.Forecast_Volume__c;
          if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 3 --- \n'
            + '\n - rfItem: ' + rfItem
            + '\n - rfItem.Forecast_Volume__c: ' + rfItem.Forecast_Volume__c
            + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
            + '\n - rfVolumeToCalculateWRQTier: ' + rfVolumeToCalculateWRQTier
            + '\n - i: ' + i
            + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
            + '\n');
          currentRoyaltyPercent = royaltyQuoteList[currentRoyaltyQuoteIndex].Royalty_Percent__c / 100;
          // checks if current Royalty Forecast volume + cumulative RF volume falls under current Royalty Quote tier
          if (totalRoyaltyForecastVolume + rfVolumeToCalculateWRQTier < royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c
            || currentRoyaltyQuoteIndex == royaltyQuoteList.size() - 1
            ) {
            currentYearRoyaltyForecastVolume = rfVolumeToCalculateWRQTier;
            currentYearRoyaltyForecastAmount += rfVolumeToCalculateWRQTier * currentRoyaltyPercent * coreMultiplier;
            totalRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;
            if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 4 --- \n'
              + '\n - currentRoyaltyPercent: ' + currentRoyaltyPercent
              + '\n - currentYearRoyaltyForecastAmount: ' + currentYearRoyaltyForecastAmount
              + '\n - i: ' + i
              + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
              + '\n');
          } else {
            // get part of current Royalty Forecast Volume that falls under current Royalty Quote tier
            rfVolumeToCalculateWRQTier = royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c - totalRoyaltyForecastVolume;
            currentYearRoyaltyForecastVolume = rfVolumeToCalculateWRQTier;
            currentYearRoyaltyForecastAmount += rfVolumeToCalculateWRQTier * currentRoyaltyPercent * coreMultiplier;

            if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 5 --- \n'
              + '\n - rfVolumeToCalculateWRQTier: ' + rfVolumeToCalculateWRQTier
              + '\n - currentYearRoyaltyForecastAmount: ' + currentYearRoyaltyForecastAmount
              + '\n - i: ' + i
              + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
              + '\n - currentYearRoyaltyForecastVolume: ' + currentYearRoyaltyForecastVolume
              + '\n');

            // add currently processed Royalty Forecast Volume to total (cumulative) Royalty Forecast Volume for all years
            totalRoyaltyForecastVolume += currentYearRoyaltyForecastVolume;

            // first Forecast volume was calculated with one Royalty Quote
            // another part will be calculated with another one
            rfVolumeToCalculateWRQTier = rfItem.Forecast_Volume__c - currentYearRoyaltyForecastVolume;

            if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 6 --- \n'
              + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
              + '\n - rfVolumeToCalculateWRQTier: ' + rfVolumeToCalculateWRQTier
              + '\n - i: ' + i
              + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
              + '\n');

            // after calculating Royalty Forecast amount for the part which falls under current Royalty Quote
            // increase counter to work with next part
            if (currentRoyaltyQuoteIndex < royaltyQuoteList.size() - 1) {
              currentRoyaltyQuoteIndex++;
              currentRoyaltyPercent = royaltyQuoteList[currentRoyaltyQuoteIndex].Royalty_Percent__c / 100;
            }
            // now check next Royalty Quote
            if (totalRoyaltyForecastVolume + rfVolumeToCalculateWRQTier < royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c) {
              currentYearRoyaltyForecastAmount += rfVolumeToCalculateWRQTier * currentRoyaltyPercent * coreMultiplier;
              totalRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;
              currentYearRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;

              if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 7 --- \n'
                + '\n - currentYearRoyaltyForecastVolume: ' + currentYearRoyaltyForecastVolume
                + '\n - currentRoyaltyPercent: ' + currentRoyaltyPercent
                + '\n - currentYearRoyaltyForecastAmount: ' + currentYearRoyaltyForecastAmount
                + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
                + '\n - i: ' + i
                + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
                + '\n');
            } else {
              if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 8 --- \n'
                + '\n - royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c: ' + royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c
                + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
                + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
                + '\n - i: ' + i
                + '\n');

              // get part of current Royalty Forecast Volume that falls under current Royalty Quote tier
              rfVolumeToCalculateWRQTier = royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c - totalRoyaltyForecastVolume;
              currentYearRoyaltyForecastAmount += rfVolumeToCalculateWRQTier * currentRoyaltyPercent * coreMultiplier;

              if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 9 --- \n'
                + '\n - currentYearRoyaltyForecastAmount: ' + currentYearRoyaltyForecastAmount
                + '\n - rfVolumeToCalculateWRQTier: ' + rfVolumeToCalculateWRQTier
                + '\n - currentRoyaltyPercent: ' + currentRoyaltyPercent
                + '\n - coreMultiplier: ' + coreMultiplier
                + '\n - i: ' + i
                + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
                + '\n - currentYearRoyaltyForecastVolume: ' + currentYearRoyaltyForecastVolume
                + '\n');

              // add currently processed Royalty Forecast Volume to total (cumulative) Royalty Forecast Volume for all years
              totalRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;
              currentYearRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;

              // first Forecast volume was calculated with one Royalty Quote
              // another part will be calculated with another one
              rfVolumeToCalculateWRQTier = rfItem.Forecast_Volume__c - currentYearRoyaltyForecastVolume;

              if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 10 --- \n'
                + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
                + '\n - rfItem.Forecast_Volume__c: ' + rfItem.Forecast_Volume__c
                + '\n - rfVolumeToCalculateWRQTier: ' + rfVolumeToCalculateWRQTier
                + '\n - i: ' + i
                + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
                + '\n - currentYearRoyaltyForecastVolume: ' + currentYearRoyaltyForecastVolume
                + '\n');

              // after calculating Royalty Forecast amount for the part which falls under current Royalty Quote
              // increase counter to work with next part
              if (currentRoyaltyQuoteIndex < royaltyQuoteList.size() - 1) {
                currentRoyaltyQuoteIndex++;
                currentRoyaltyPercent = royaltyQuoteList[currentRoyaltyQuoteIndex].Royalty_Percent__c / 100;
              }
              // now check next Royalty Quote
              if (rfItem.Forecast_Volume__c + currentYearRoyaltyForecastVolume < royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c) {
                currentYearRoyaltyForecastAmount += rfVolumeToCalculateWRQTier * currentRoyaltyPercent * coreMultiplier;
                totalRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;

                if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 11 --- \n'
                  + '\n - currentRoyaltyPercent: ' + currentRoyaltyPercent
                  + '\n - currentYearRoyaltyForecastAmount: ' + currentYearRoyaltyForecastAmount
                  + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
                  + '\n - i: ' + i
                  + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
                  + '\n');
              } else {
                currentYearRoyaltyForecastAmount += rfVolumeToCalculateWRQTier * currentRoyaltyPercent * coreMultiplier;
                totalRoyaltyForecastVolume += rfVolumeToCalculateWRQTier;

                if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 12 --- \n'
                  + '\n - currentRoyaltyPercent: ' + currentRoyaltyPercent
                  + '\n - currentYearRoyaltyForecastAmount: ' + currentYearRoyaltyForecastAmount
                  + '\n - totalRoyaltyForecastVolume: ' + totalRoyaltyForecastVolume
                  + '\n - i: ' + i
                  + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
                  + '\n');
              }

            }

          }


          rfItem.Royalty_Forecast__c = currentYearRoyaltyForecastAmount;
          rfItem.Royalty_Rate_ASP__c = royaltyQuoteList[currentRoyaltyQuoteIndex].Royalty_Percent__c;

          if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 13 --- \n'
            + '\n - rfItem.Forecast_Volume__c: ' + rfItem.Forecast_Volume__c
            + '\n - rfItem.Royalty_Forecast__c: ' + rfItem.Royalty_Forecast__c
            + '\n - rfItem.Royalty_Rate_ASP__c: ' + rfItem.Royalty_Rate_ASP__c
            + '\n - i: ' + i
            + '\n - currentRoyaltyQuoteIndex: ' + currentRoyaltyQuoteIndex
            + '\n');

          /*if (isCurrentForecastVolumeInOneRoyaltyRange(totalRoyaltyForecastAmount, rfItem.Forecast_Volume__c, royaltyQuoteList)) {
          Decimal royaltyForecastRate = getRoyaltyPercentFromVolume((totalRoyaltyForecastAmount + rfItem.Forecast_Volume__c), royaltyQuoteList);
          if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 3.1 --- \n'
            + '\n - rfItem: ' + rfItem
            + '\n - totalRoyaltyForecastAmount: ' + totalRoyaltyForecastAmount
            + '\n - rfItem.Forecast_Volume__c: ' + rfItem.Forecast_Volume__c
            + '\n - royaltyForecastRate: ' + royaltyForecastRate
            + '\n');
          rfItem.Royalty_Forecast__c = rfItem.Forecast_Volume__c * coreMultiplier * royaltyForecastRate;
          rfItem.Royalty_Rate_ASP__c = royaltyForecastRate;
          if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 3.2 --- \n'
            + '\n - totalRoyaltyForecastAmount: ' + totalRoyaltyForecastAmount
            + '\n - rfItem.Royalty_Forecast__c: ' + rfItem.Royalty_Forecast__c
            + '\n');
        } else {*/
          /*if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 4.1 --- \n'
          + '\n - i: ' + i
          + '\n - royaltyQuoteList[i]: ' + (i < royaltyQuoteList.size() ? String.valueOf(royaltyQuoteList[j]) : '')
          + '\n - sortedRoyaltyForecasts[i]: ' + (i < sortedRoyaltyForecasts.size() ? String.valueOf(sortedRoyaltyForecasts[i]) : '')
          + '\n');
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 4.2 --- \n'
          + '\n - royaltyQuoteList[i-1].Volume_Max__c: ' + (i > 0 ? String.valueOf(royaltyQuoteList[j - 1].Volume_Max__c) : '')
          + '\n - sortedRoyaltyForecasts[i-1].Forecast_Volume__c: ' + (i > 0 ? String.valueOf(sortedRoyaltyForecasts[i - 1].Forecast_Volume__c) : '')
          + '\n - coreMultiplier: ' + coreMultiplier
          + '\n - royaltyQuoteList[i-1].Royalty_Percent__c: ' + (i > 0 ? String.valueOf(royaltyQuoteList[j - 1].Royalty_Percent__c) : '')
          + '\n - sortedRoyaltyForecasts[i].Forecast_Volume__c: ' + (i < sortedRoyaltyForecasts.size() ? String.valueOf(sortedRoyaltyForecasts[i].Forecast_Volume__c) : '')
          + '\n - royaltyQuoteList[i-1].Volume_Max__c: ' + (i > 0 ? String.valueOf(royaltyQuoteList[j - 1].Volume_Max__c) : '')
          + '\n - coreMultiplier: ' + coreMultiplier
          + '\n - royaltyQuoteList[i].Royalty_Percent__c: ' + (i < royaltyQuoteList.size() ? String.valueOf(royaltyQuoteList[j].Royalty_Percent__c) : '')
          + '\n');
        Decimal royalForecast02 = 0;
        if (i > 0) {
          royalForecast02 =
            ((royaltyQuoteList[j - 1].Volume_Max__c - sortedRoyaltyForecasts[i - 1].Forecast_Volume__c) * coreMultiplier * royaltyQuoteList[j - 1].Royalty_Percent__c / 100)
              + ((sortedRoyaltyForecasts[i].Forecast_Volume__c - royaltyQuoteList[j - 1].Volume_Max__c) * coreMultiplier * royaltyQuoteList[j].Royalty_Percent__c / 100);
        } else {
          royalForecast02 = rfItem.Forecast_Volume__c * coreMultiplier * royaltyQuoteList[j].Royalty_Percent__c / 100;
        }
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler - calculateRoyaltyRate - 4.2 --- \n'
          + '\n - royalForecast02: ' + royalForecast02
          + '\n');

        rfItem.Royalty_Forecast__c = royalForecast02;
        rfItem.Royalty_Rate_ASP__c = royaltyQuoteList[j].Royalty_Percent__c;

        //}
        totalRoyaltyForecastAmount += rfItem.Forecast_Volume__c;*/
          i++;
        }
      } else if (royaltyQuoteList[0].Royalties_Type__c == 'Fixed') {
        for (Royalty_Forecast__c rfItem : sortedRoyaltyForecasts) {
          rfItem.Royalty_Forecast__c = royaltyQuoteList[0].Fixed_Royalties_Amount__c * rfItem.Forecast_Volume__c;
        }
      }
    }
  }


  /*private static Decimal getQwerty(
    Decimal totalRoyaltyForecastVolume,
    Royalty_Forecast__c rfItem,
    List<Royalty_Quote__c> royaltyQuoteList,
    Integer currentRoyaltyQuoteIndex,
    Decimal currentRoyaltyPercent,
    Decimal currentYearRoyaltyForecastAmount,
    Decimal coreMultiplier
  ) {
    Decimal internalCurrentYearRoyaltyForecastAmount = 0;

    if (totalRoyaltyForecastVolume + rfItem.Forecast_Volume__c < royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c) {
      currentRoyaltyPercent = royaltyQuoteList[currentRoyaltyQuoteIndex].Royalty_Percent__c / 100;

      internalCurrentYearRoyaltyForecastAmount += rfItem.Forecast_Volume__c * currentRoyaltyPercent * coreMultiplier;
    } else {

      // get part of current Royalty Forecast Volume that falls under current Royalty Quote tier
      Decimal rfVolumeToCalculateWCurrentRQTier = royaltyQuoteList[currentRoyaltyQuoteIndex].Volume_Max__c - totalRoyaltyForecastVolume;
      internalCurrentYearRoyaltyForecastAmount += rfVolumeToCalculateWCurrentRQTier * currentRoyaltyPercent * coreMultiplier;

      currentRoyaltyQuoteIndex++;

    }

    return internalCurrentYearRoyaltyForecastAmount;
  }*/


  private static List<Number_of_cores_to_Multiplier__mdt> numberOfCoresToMultipliers;
  @testVisible
  private static Decimal getNoOfCoresMultiplierForRF(Royalty_Quote__c rqItem) {
    if (numberOfCoresToMultipliers == null) {
      numberOfCoresToMultipliers = [
        SELECT Min__c, Max__c, Multiplier__c
        FROM Number_of_cores_to_Multiplier__mdt
        ORDER BY Multiplier__c ASC
      ];
    }
    for (Number_of_cores_to_Multiplier__mdt multiplierItem : numberOfCoresToMultipliers) {
      if (multiplierItem.Min__c <= rqItem.N_of_Instantiations__c && rqItem.N_of_Instantiations__c <= multiplierItem.Max__c) {
        return multiplierItem.Multiplier__c;
      }
    }
    return 1;
  }

  @testVisible
  private static Boolean isCurrentForecastVolumeInOneRoyaltyRange(Decimal totalRoyaltyForecastAmount, Decimal forecastVolume, List<Royalty_Quote__c> forecastRoyaltyQuotes) {
    for (Royalty_Quote__c fqItem : forecastRoyaltyQuotes) {
      if (totalRoyaltyForecastAmount == 0) {
        if (fqItem.Volume_Min__c <= forecastVolume && forecastVolume <= fqItem.Volume_Max__c) {
          return true;
        }
      } else if (fqItem.Volume_Min__c <= totalRoyaltyForecastAmount
        && totalRoyaltyForecastAmount <= fqItem.Volume_Max__c
        && fqItem.Volume_Min__c <= (forecastVolume + totalRoyaltyForecastAmount)
        && (forecastVolume + totalRoyaltyForecastAmount) <= fqItem.Volume_Max__c
        ) {
        return true;
      }
    }
    return false;
  }

  @testVisible
  private static Decimal getRoyaltyPercentFromVolume(Decimal forecastVolume, List<Royalty_Quote__c> forecastRoyaltyQuotes) {
    for (Royalty_Quote__c fqItem : forecastRoyaltyQuotes) {
      if (fqItem.Volume_Min__c <= forecastVolume && forecastVolume <= fqItem.Volume_Max__c) {
        return fqItem.Royalty_Percent__c * 0.01;
      }
    }
    return 0.01;
  }


  @testVisible
  private static List<Royalty_Forecast__c> getSortedRoyaltyForecastList(List<Royalty_Forecast__c> unsortedList) {
    List<Royalty_Forecast__c> sortedList = new List<Royalty_Forecast__c>();

    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists01 = new List<List<Royalty_Forecast__c>>();
    for (Royalty_Forecast__c rfItem : unsortedList) {
      listOfSortedRoyaltyForecastLists01.add(new List<Royalty_Forecast__c>{rfItem});
    }
    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists02 = getSortedRoyaltyForecastListOfLists(listOfSortedRoyaltyForecastLists01);

    return listOfSortedRoyaltyForecastLists02[0];
  }

  @testVisible
  private static Boolean isDebug = true;
  @testVisible
  private static List<List<Royalty_Forecast__c>> getSortedRoyaltyForecastListOfLists(List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists01) {
    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists02 = new List<List<Royalty_Forecast__c>>();

    if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 1 --- \n'
      + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
      + '\n');

    while (!listOfSortedRoyaltyForecastLists01.isEmpty()) {
      List<Royalty_Forecast__c> tempList;

      if (listOfSortedRoyaltyForecastLists01.size() > 1) {
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 2 --- \n'
          + '\n - listOfSortedRoyaltyForecastLists01.size(): ' + listOfSortedRoyaltyForecastLists01.size()
          + '\n');
        tempList = sortMergeRoyaltyForecastLists(listOfSortedRoyaltyForecastLists01[0], listOfSortedRoyaltyForecastLists01[1]);
        listOfSortedRoyaltyForecastLists01.remove(1);
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 3 --- \n'
          + '\n - tempList: ' + tempList
          + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
          + '\n');
      } else {
        tempList = listOfSortedRoyaltyForecastLists01[0];
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 4 --- \n'
          + '\n - tempList: ' + tempList
          + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
          + '\n');
      }
      listOfSortedRoyaltyForecastLists01.remove(0);

      listOfSortedRoyaltyForecastLists02.add(tempList);
      if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 5 --- \n'
        + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
        + '\n - listOfSortedRoyaltyForecastLists02: ' + listOfSortedRoyaltyForecastLists02
        + '\n');
    }

    if (listOfSortedRoyaltyForecastLists02.size() > 1) {
      if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 6 --- \n'
        + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
        + '\n - listOfSortedRoyaltyForecastLists02: ' + listOfSortedRoyaltyForecastLists02
        + '\n');
      listOfSortedRoyaltyForecastLists02 = getSortedRoyaltyForecastListOfLists(listOfSortedRoyaltyForecastLists02);
      if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 7 --- \n'
        + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
        + '\n - listOfSortedRoyaltyForecastLists02: ' + listOfSortedRoyaltyForecastLists02
        + '\n');
    }

    if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- getSortedRoyaltyForecastListOfLists - 8 --- \n'
      + '\n - listOfSortedRoyaltyForecastLists01: ' + listOfSortedRoyaltyForecastLists01
      + '\n - listOfSortedRoyaltyForecastLists02: ' + listOfSortedRoyaltyForecastLists02
      + '\n');
    return listOfSortedRoyaltyForecastLists02;
  }

  @testVisible
  private static List<Royalty_Forecast__c> sortMergeRoyaltyForecastLists(List<Royalty_Forecast__c> listA, List<Royalty_Forecast__c> listB) {
    List<Royalty_Forecast__c> sortedMergedListC = new List<Royalty_Forecast__c>();

    if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- sortMergeRoyaltyForecastLists - 1 --- \n'
      + '\n - listA: ' + listA
      + '\n - listB: ' + listB
      + '\n');

    while (!listA.isEmpty() && !listB.isEmpty()) {
      if (listA[0].Forecast_Year__c <= listB[0].Forecast_Year__c) {
        sortedMergedListC.add(listA[0]);
        listA.remove(0);
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- sortMergeRoyaltyForecastLists - 2 --- \n'
          + '\n - listA: ' + listA
          + '\n - listB: ' + listB
          + '\n - sortedMergedListC: ' + sortedMergedListC
          + '\n');
      } else {
        sortedMergedListC.add(listB[0]);
        listB.remove(0);
        if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- sortMergeRoyaltyForecastLists - 3 --- \n'
          + '\n - listA: ' + listA
          + '\n - listB: ' + listB
          + '\n - sortedMergedListC: ' + sortedMergedListC
          + '\n');
      }
    }

    while (!listA.isEmpty()) {
      sortedMergedListC.add(listA[0]);
      listA.remove(0);
      if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- sortMergeRoyaltyForecastLists - 4 --- \n'
        + '\n - listA: ' + listA
        + '\n - listB: ' + listB
        + '\n - sortedMergedListC: ' + sortedMergedListC
        + '\n');
    }
    while (!listB.isEmpty()) {
      sortedMergedListC.add(listB[0]);
      listB.remove(0);
      if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- sortMergeRoyaltyForecastLists - 5 --- \n'
        + '\n - listA: ' + listA
        + '\n - listB: ' + listB
        + '\n - sortedMergedListC: ' + sortedMergedListC
        + '\n');
    }
    if (isDebug) System.debug(LoggingLevel.DEBUG, '\n\n --- sortMergeRoyaltyForecastLists - 6 --- \n'
      + '\n - listA: ' + listA
      + '\n - listB: ' + listB
      + '\n - sortedMergedListC: ' + sortedMergedListC
      + '\n');
    return sortedMergedListC;
  }

}