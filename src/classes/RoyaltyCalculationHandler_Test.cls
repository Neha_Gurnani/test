@IsTest
private class RoyaltyCalculationHandler_Test {

  @isTest
  static void sortMergeRoyaltyForecastLists2Items() {
    Royalty_Forecast__c rf01 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today()
    );
    Royalty_Forecast__c rf02 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1)
    );

    // 'sorted' lists
    List<Royalty_Forecast__c> listA = new List<Royalty_Forecast__c>{rf01};
    List<Royalty_Forecast__c> listB = new List<Royalty_Forecast__c>{rf02};

    List<Royalty_Forecast__c> listC = RoyaltyCalculationHandler.sortMergeRoyaltyForecastLists(listA, listB);
    System.assertEquals(rf01.Forecast_Year__c, listC[0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listC[1].Forecast_Year__c);

    // 'unsorted' lists
    List<Royalty_Forecast__c> listA02 = new List<Royalty_Forecast__c>{rf02};
    List<Royalty_Forecast__c> listB02 = new List<Royalty_Forecast__c>{rf01};

    List<Royalty_Forecast__c> listC02 = RoyaltyCalculationHandler.sortMergeRoyaltyForecastLists(listA02, listB02);
    System.assertEquals(rf01.Forecast_Year__c, listC02[0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listC02[1].Forecast_Year__c);
  }

  @isTest
  static void sortMergeRoyaltyForecastLists3Items() {
    Royalty_Forecast__c rf01 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today()
    );
    Royalty_Forecast__c rf02 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1)
    );
    Royalty_Forecast__c rf03 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2)
    );

    // 'sorted' lists
    List<Royalty_Forecast__c> listA = new List<Royalty_Forecast__c>{rf01, rf03};
    List<Royalty_Forecast__c> listB = new List<Royalty_Forecast__c>{rf02};

    List<Royalty_Forecast__c> listC = RoyaltyCalculationHandler.sortMergeRoyaltyForecastLists(listA, listB);
    System.assertEquals(rf01.Forecast_Year__c, listC[0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listC[1].Forecast_Year__c);
    System.assertEquals(rf03.Forecast_Year__c, listC[2].Forecast_Year__c);

    // 'unsorted' lists
    List<Royalty_Forecast__c> listA02 = new List<Royalty_Forecast__c>{rf02};
    List<Royalty_Forecast__c> listB02 = new List<Royalty_Forecast__c>{rf01, rf03};

    List<Royalty_Forecast__c> listC02 = RoyaltyCalculationHandler.sortMergeRoyaltyForecastLists(listA02, listB02);
    System.assertEquals(rf01.Forecast_Year__c, listC02[0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listC02[1].Forecast_Year__c);
    System.assertEquals(rf03.Forecast_Year__c, listC02[2].Forecast_Year__c);
  }

  @isTest
  static void getSortedRoyaltyForecastListOfLists3Items() {
    Royalty_Forecast__c rf01 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today()
    );
    Royalty_Forecast__c rf02 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1)
    );
    Royalty_Forecast__c rf03 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2)
    );

    // 'sorted' lists
    List<Royalty_Forecast__c> listA = new List<Royalty_Forecast__c>{rf01, rf03};
    List<Royalty_Forecast__c> listB = new List<Royalty_Forecast__c>{rf02};
    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists01 = new List<List<Royalty_Forecast__c>>();
    listOfSortedRoyaltyForecastLists01.add(listA);
    listOfSortedRoyaltyForecastLists01.add(listB);

    listOfSortedRoyaltyForecastLists01 = RoyaltyCalculationHandler.getSortedRoyaltyForecastListOfLists(listOfSortedRoyaltyForecastLists01);
    System.assertEquals(1, listOfSortedRoyaltyForecastLists01.size());
    System.assertEquals(3, listOfSortedRoyaltyForecastLists01[0].size());
    System.assertEquals(rf01.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][1].Forecast_Year__c);
    System.assertEquals(rf03.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][2].Forecast_Year__c);

    // 'unsorted' lists
    List<Royalty_Forecast__c> listA02 = new List<Royalty_Forecast__c>{rf02};
    List<Royalty_Forecast__c> listB02 = new List<Royalty_Forecast__c>{rf01, rf03};
    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists02 = new List<List<Royalty_Forecast__c>>();
    listOfSortedRoyaltyForecastLists02.add(listA02);
    listOfSortedRoyaltyForecastLists02.add(listB02);

    listOfSortedRoyaltyForecastLists02 = RoyaltyCalculationHandler.getSortedRoyaltyForecastListOfLists(listOfSortedRoyaltyForecastLists02);
    System.assertEquals(1, listOfSortedRoyaltyForecastLists02.size());
    System.assertEquals(3, listOfSortedRoyaltyForecastLists02[0].size());
    System.assertEquals(rf01.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][1].Forecast_Year__c);
    System.assertEquals(rf03.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][2].Forecast_Year__c);
  }

  @isTest
  static void getSortedRoyaltyForecastListOfLists4Items() {
    Royalty_Forecast__c rf01 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today()
    );
    Royalty_Forecast__c rf02 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1)
    );
    Royalty_Forecast__c rf03 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2)
    );
    Royalty_Forecast__c rf04 = new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(3)
    );

    // 'sorted' lists
    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists01 = new List<List<Royalty_Forecast__c>>();
    listOfSortedRoyaltyForecastLists01.add(new List<Royalty_Forecast__c>{rf01});
    listOfSortedRoyaltyForecastLists01.add(new List<Royalty_Forecast__c>{rf03});
    listOfSortedRoyaltyForecastLists01.add(new List<Royalty_Forecast__c>{rf02});
    listOfSortedRoyaltyForecastLists01.add(new List<Royalty_Forecast__c>{rf04});

    listOfSortedRoyaltyForecastLists01 = RoyaltyCalculationHandler.getSortedRoyaltyForecastListOfLists(listOfSortedRoyaltyForecastLists01);

    System.assertEquals(1, listOfSortedRoyaltyForecastLists01.size());
    System.assertEquals(4, listOfSortedRoyaltyForecastLists01[0].size());
    System.assertEquals(rf01.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][1].Forecast_Year__c);
    System.assertEquals(rf03.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][2].Forecast_Year__c);
    System.assertEquals(rf04.Forecast_Year__c, listOfSortedRoyaltyForecastLists01[0][3].Forecast_Year__c);

    // 'unsorted' lists
    List<List<Royalty_Forecast__c>> listOfSortedRoyaltyForecastLists02 = new List<List<Royalty_Forecast__c>>();
    listOfSortedRoyaltyForecastLists02.add(new List<Royalty_Forecast__c>{rf02});
    listOfSortedRoyaltyForecastLists02.add(new List<Royalty_Forecast__c>{rf01});
    listOfSortedRoyaltyForecastLists02.add(new List<Royalty_Forecast__c>{rf04});
    listOfSortedRoyaltyForecastLists02.add(new List<Royalty_Forecast__c>{rf03});

    listOfSortedRoyaltyForecastLists02 = RoyaltyCalculationHandler.getSortedRoyaltyForecastListOfLists(listOfSortedRoyaltyForecastLists02);

    System.assertEquals(1, listOfSortedRoyaltyForecastLists02.size());
    System.assertEquals(4, listOfSortedRoyaltyForecastLists02[0].size());
    System.assertEquals(rf01.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][0].Forecast_Year__c);
    System.assertEquals(rf02.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][1].Forecast_Year__c);
    System.assertEquals(rf03.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][2].Forecast_Year__c);
    System.assertEquals(rf04.Forecast_Year__c, listOfSortedRoyaltyForecastLists02[0][3].Forecast_Year__c);
  }


  @isTest
  static void getNoOfCoresMultiplierForRF() {
    List<Number_of_cores_to_Multiplier__mdt> numberOfCoresToMultipliers = [
      SELECT Min__c, Max__c, Multiplier__c
      FROM Number_of_cores_to_Multiplier__mdt
      ORDER BY Multiplier__c ASC
    ];

    if (numberOfCoresToMultipliers.size() > 1) {
      Decimal numberOfCores = numberOfCoresToMultipliers[1].Min__c;
      Decimal multiplier = numberOfCoresToMultipliers[1].Multiplier__c;
      Decimal testMultiplier = RoyaltyCalculationHandler.getNoOfCoresMultiplierForRF(new Royalty_Quote__c(
        N_of_Instantiations__c = numberOfCores
      ));
      System.assertEquals(multiplier, testMultiplier);
    }
  }


  @isTest
  static void getRoyaltyPercentFromVolume() {
    List<Royalty_Quote__c> forecastRoyaltyQuotes = new List<Royalty_Quote__c>{
      new Royalty_Quote__c(Volume_Min__c = 1, Volume_Max__c = 10, Royalty_Percent__c = 2),
      new Royalty_Quote__c(Volume_Min__c = 11, Volume_Max__c = 100, Royalty_Percent__c = 22)
    };

    System.assertEquals(0.02, RoyaltyCalculationHandler.getRoyaltyPercentFromVolume(2, forecastRoyaltyQuotes));
    System.assertEquals(0.22, RoyaltyCalculationHandler.getRoyaltyPercentFromVolume(22, forecastRoyaltyQuotes));
  }


  @isTest
  static void isCurrentForecastVolumeInOneRoyaltyRange() {
    List<Royalty_Quote__c> forecastRoyaltyQuotes = new List<Royalty_Quote__c>{
      new Royalty_Quote__c(Volume_Min__c = 1, Volume_Max__c = 10, Royalty_Percent__c = 2),
      new Royalty_Quote__c(Volume_Min__c = 11, Volume_Max__c = 100, Royalty_Percent__c = 22)
    };
    Decimal totalRoyaltyForecastVolume = 0;
    Decimal forecastVolume = 5;
    System.assertEquals(true, RoyaltyCalculationHandler.isCurrentForecastVolumeInOneRoyaltyRange(totalRoyaltyForecastVolume, forecastVolume, forecastRoyaltyQuotes));
    totalRoyaltyForecastVolume = 5;
    forecastVolume = 15;
    System.assertEquals(false, RoyaltyCalculationHandler.isCurrentForecastVolumeInOneRoyaltyRange(totalRoyaltyForecastVolume, forecastVolume, forecastRoyaltyQuotes));
  }


  @isTest
  static void calculateRoyaltyRate() {
    Account douLLC = new Account(Name = 'Dou LLC');
    insert douLLC;
    Opportunity opp01 = new Opportunity(
      Name = '01',
      AccountId = douLLC.Id,
      StageName = Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue(),
      CloseDate = Date.today().addDays(3)
    );
    insert opp01;

    List<Royalty_Quote__c> forecastRoyaltyQuotes = new List<Royalty_Quote__c>{
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1, Volume_Max__c = 1000000, Royalty_Percent__c = 2.5),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1000001, Volume_Max__c = 4000000, Royalty_Percent__c = 2.25),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 4000001, Volume_Max__c = 10000000, Royalty_Percent__c = 2)
    };
    insert forecastRoyaltyQuotes;

    Test.startTest();

    List<Royalty_Forecast__c> listToInsert = new List<Royalty_Forecast__c>();
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today(),
      Forecast_Volume__c = 900000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1),
      Forecast_Volume__c = 1500000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2),
      Forecast_Volume__c = 2000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(3),
      Forecast_Volume__c = 2000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(4),
      Forecast_Volume__c = 1000000,
      Opportunity__c = opp01.Id
    ));
    RoyaltyCalculationHandler.isDebug = true;
    insert listToInsert;

    List<Number_of_cores_to_Multiplier__mdt> numberOfCoresToMultipliers = [
      SELECT Min__c, Max__c, Multiplier__c
      FROM Number_of_cores_to_Multiplier__mdt
      ORDER BY Multiplier__c ASC
    ];
    Decimal coreMultiplier;
    if (numberOfCoresToMultipliers[0].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[0].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[0].Multiplier__c;
    } else if (numberOfCoresToMultipliers[1].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[1].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[1].Multiplier__c;
    }

    listToInsert = [
      SELECT Royalty_Forecast__c, Royalty_Rate_ASP__c, Forecast_Volume__c
      FROM Royalty_Forecast__c
      WHERE Opportunity__c = :opp01.Id
      ORDER BY Forecast_Year__c ASC
    ];

    Decimal totalRoyaltyForecastVolume = 0;
    Decimal currentRoyaltyForecastAmount = (listToInsert[0].Forecast_Volume__c - totalRoyaltyForecastVolume) * coreMultiplier * forecastRoyaltyQuotes[0].Royalty_Percent__c / 100;
    totalRoyaltyForecastVolume = totalRoyaltyForecastVolume + currentRoyaltyForecastAmount;
    Decimal royalForecast01 = currentRoyaltyForecastAmount;
    System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler_Test - calculateRoyaltyRate - 1 --- \n'
      + '\n - royalForecast01: ' + royalForecast01
      + '\n - listToInsert[0].Royalty_Forecast__c: ' + listToInsert[0].Royalty_Forecast__c
      + '\n');
    System.assertEquals(royalForecast01, listToInsert[0].Royalty_Forecast__c);

    currentRoyaltyForecastAmount = listToInsert[1].Forecast_Volume__c * coreMultiplier * forecastRoyaltyQuotes[1].Royalty_Percent__c / 100;
    Decimal royalForecast02 = (100000 * coreMultiplier * forecastRoyaltyQuotes[0].Royalty_Percent__c / 100) + (1400000 * coreMultiplier * forecastRoyaltyQuotes[1].Royalty_Percent__c / 100);
    //((forecastRoyaltyQuotes[0].Volume_Max__c - totalRoyaltyForecastVolume) * coreMultiplier * forecastRoyaltyQuotes[0].Royalty_Percent__c / 100)
    // + ((listToInsert[1].Forecast_Volume__c - forecastRoyaltyQuotes[0].Volume_Max__c - totalRoyaltyForecastVolume) * coreMultiplier * forecastRoyaltyQuotes[1].Royalty_Percent__c / 100);
    System.debug(LoggingLevel.DEBUG, '\n\n --- RoyaltyCalculationHandler_Test - calculateRoyaltyRate - 2 --- \n'
      + '\n - royalForecast02: ' + royalForecast02
      + '\n - listToInsert[1].Royalty_Forecast__c: ' + listToInsert[1].Royalty_Forecast__c
      + '\n');
    System.assertEquals(royalForecast02, listToInsert[1].Royalty_Forecast__c);
    System.assertEquals(55000, listToInsert[2].Royalty_Forecast__c);
    System.assertEquals(50000, listToInsert[3].Royalty_Forecast__c);
    System.assertEquals(25000, listToInsert[4].Royalty_Forecast__c);

    Test.stopTest();
  }

  @isTest
  static void calculateRoyaltyRate2() {
    Account douLLC = new Account(Name = 'Dou LLC');
    insert douLLC;
    Opportunity opp01 = new Opportunity(
      Name = '01',
      AccountId = douLLC.Id,
      StageName = Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue(),
      CloseDate = Date.today().addDays(3)
    );
    insert opp01;

    List<Royalty_Quote__c> forecastRoyaltyQuotes = new List<Royalty_Quote__c>{
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1, Volume_Max__c = 1000000, Royalty_Percent__c = 2.5),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1000001, Volume_Max__c = 4000000, Royalty_Percent__c = 2.25),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 4000001, Volume_Max__c = 10000000, Royalty_Percent__c = 2)
    };
    insert forecastRoyaltyQuotes;

    Test.startTest();

    List<Royalty_Forecast__c> listToInsert = new List<Royalty_Forecast__c>();
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today(),
      Forecast_Volume__c = 500000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1),
      Forecast_Volume__c = 1500000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2),
      Forecast_Volume__c = 4000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(3),
      Forecast_Volume__c = 4000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(4),
      Forecast_Volume__c = 4000000,
      Opportunity__c = opp01.Id
    ));
    RoyaltyCalculationHandler.isDebug = true;
    insert listToInsert;

    List<Number_of_cores_to_Multiplier__mdt> numberOfCoresToMultipliers = [
      SELECT Min__c, Max__c, Multiplier__c
      FROM Number_of_cores_to_Multiplier__mdt
      ORDER BY Multiplier__c ASC
    ];
    Decimal coreMultiplier;
    if (numberOfCoresToMultipliers[0].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[0].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[0].Multiplier__c;
    } else if (numberOfCoresToMultipliers[1].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[1].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[1].Multiplier__c;
    }

    listToInsert = [
      SELECT Royalty_Forecast__c, Royalty_Rate_ASP__c, Forecast_Volume__c
      FROM Royalty_Forecast__c
      WHERE Opportunity__c = :opp01.Id
      ORDER BY Forecast_Year__c ASC
    ];

    System.assertEquals(15625, listToInsert[0].Royalty_Forecast__c);
    System.assertEquals(43750, listToInsert[1].Royalty_Forecast__c);
    System.assertEquals(106250, listToInsert[2].Royalty_Forecast__c);
    System.assertEquals(100000, listToInsert[3].Royalty_Forecast__c);
    System.assertEquals(100000, listToInsert[4].Royalty_Forecast__c);

    Test.stopTest();
  }

  @isTest
  static void calculateRoyaltyRate3() {
    Account douLLC = new Account(Name = 'Dou LLC');
    insert douLLC;
    Opportunity opp01 = new Opportunity(
      Name = '01',
      AccountId = douLLC.Id,
      StageName = Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue(),
      CloseDate = Date.today().addDays(3)
    );
    insert opp01;

    List<Royalty_Quote__c> forecastRoyaltyQuotes = new List<Royalty_Quote__c>{
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1, Volume_Max__c = 1000000, Royalty_Percent__c = 2.5),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1000001, Volume_Max__c = 4000000, Royalty_Percent__c = 2.25),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 4000001, Volume_Max__c = 10000000, Royalty_Percent__c = 2)
    };
    insert forecastRoyaltyQuotes;

    Test.startTest();

    List<Royalty_Forecast__c> listToInsert = new List<Royalty_Forecast__c>();
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today(),
      Forecast_Volume__c = 500000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1),
      Forecast_Volume__c = 15500000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2),
      Forecast_Volume__c = 40000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(3),
      Forecast_Volume__c = 40000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(4),
      Forecast_Volume__c = 40000000,
      Opportunity__c = opp01.Id
    ));
    RoyaltyCalculationHandler.isDebug = true;
    insert listToInsert;

    List<Number_of_cores_to_Multiplier__mdt> numberOfCoresToMultipliers = [
      SELECT Min__c, Max__c, Multiplier__c
      FROM Number_of_cores_to_Multiplier__mdt
      ORDER BY Multiplier__c ASC
    ];
    Decimal coreMultiplier;
    if (numberOfCoresToMultipliers[0].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[0].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[0].Multiplier__c;
    } else if (numberOfCoresToMultipliers[1].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[1].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[1].Multiplier__c;
    }

    listToInsert = [
      SELECT Royalty_Forecast__c, Royalty_Rate_ASP__c, Forecast_Volume__c
      FROM Royalty_Forecast__c
      WHERE Opportunity__c = :opp01.Id
      ORDER BY Forecast_Year__c ASC
    ];

    System.assertEquals(15625, listToInsert[0].Royalty_Forecast__c);
    System.assertEquals(400000, listToInsert[1].Royalty_Forecast__c);
    System.assertEquals(1000000, listToInsert[2].Royalty_Forecast__c);
    System.assertEquals(1000000, listToInsert[3].Royalty_Forecast__c);
    System.assertEquals(1000000, listToInsert[4].Royalty_Forecast__c);

    Test.stopTest();
  }
    
    
  @isTest
  static void calculateRoyaltyRateInsertOneByOne() {
    Account douLLC = new Account(Name = 'Dou LLC');
    insert douLLC;
    Opportunity opp01 = new Opportunity(
      Name = '01',
      AccountId = douLLC.Id,
      StageName = Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue(),
      CloseDate = Date.today().addDays(3)
    );
    insert opp01;

    List<Royalty_Quote__c> forecastRoyaltyQuotes = new List<Royalty_Quote__c>{
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1, Volume_Max__c = 1000000, Royalty_Percent__c = 2.5),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 1000001, Volume_Max__c = 4000000, Royalty_Percent__c = 2.25),
      new Royalty_Quote__c(Opportunity__c = opp01.Id, N_of_Instantiations__c = 2, Volume_Min__c = 4000001, Volume_Max__c = 10000000, Royalty_Percent__c = 2)
    };
    insert forecastRoyaltyQuotes;

    Test.startTest();

      //RoyaltyCalculationHandler.isDebug = true;
    insert new Royalty_Forecast__c(
      Forecast_Year__c = Date.today(),
      Forecast_Volume__c = 500000,
      Opportunity__c = opp01.Id
    );
    insert new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1),
      Forecast_Volume__c = 15500000,
      Opportunity__c = opp01.Id
    );
    insert new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2),
      Forecast_Volume__c = 40000000,
      Opportunity__c = opp01.Id
    );
    insert new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(3),
      Forecast_Volume__c = 40000000,
      Opportunity__c = opp01.Id
    );
    insert new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(4),
      Forecast_Volume__c = 40000000,
      Opportunity__c = opp01.Id
    );
    
    List<Number_of_cores_to_Multiplier__mdt> numberOfCoresToMultipliers = [
      SELECT Min__c, Max__c, Multiplier__c
      FROM Number_of_cores_to_Multiplier__mdt
      ORDER BY Multiplier__c ASC
    ];
    Decimal coreMultiplier;
    if (numberOfCoresToMultipliers[0].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[0].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[0].Multiplier__c;
    } else if (numberOfCoresToMultipliers[1].Min__c <= forecastRoyaltyQuotes[0].N_of_Instantiations__c && forecastRoyaltyQuotes[0].N_of_Instantiations__c <= numberOfCoresToMultipliers[1].Max__c) {
      coreMultiplier = numberOfCoresToMultipliers[1].Multiplier__c;
    }

    List<Royalty_Forecast__c> listToInsert = [
      SELECT Royalty_Forecast__c, Royalty_Rate_ASP__c, Forecast_Volume__c
      FROM Royalty_Forecast__c
      WHERE Opportunity__c = :opp01.Id
      ORDER BY Forecast_Year__c ASC
    ];

    System.assertEquals(15625, listToInsert[0].Royalty_Forecast__c);
    System.assertEquals(400000, listToInsert[1].Royalty_Forecast__c);
    System.assertEquals(1000000, listToInsert[2].Royalty_Forecast__c);
    System.assertEquals(1000000, listToInsert[3].Royalty_Forecast__c);
    System.assertEquals(1000000, listToInsert[4].Royalty_Forecast__c);

    Test.stopTest();
  }


  @isTest
  static void calculateFixedRoyaltyRate() {
    Account douLLC = new Account(Name = 'Dou LLC');
    insert douLLC;
    Opportunity opp01 = new Opportunity(
      Name = '01',
      AccountId = douLLC.Id,
      StageName = Opportunity.StageName.getDescribe().getPicklistValues()[0].getValue(),
      CloseDate = Date.today().addDays(3)
    );
    insert opp01;

    Decimal fixedRoyaltyAmount = 10;
    insert new Royalty_Quote__c(Opportunity__c = opp01.Id, Royalties_Type__c = 'Fixed', Fixed_Royalties_Amount__c = fixedRoyaltyAmount);

    Test.startTest();

    List<Royalty_Forecast__c> listToInsert = new List<Royalty_Forecast__c>();
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today(),
      Forecast_Volume__c = 900000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(1),
      Forecast_Volume__c = 1500000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(2),
      Forecast_Volume__c = 2000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(3),
      Forecast_Volume__c = 2000000,
      Opportunity__c = opp01.Id
    ));
    listToInsert.add(new Royalty_Forecast__c(
      Forecast_Year__c = Date.today().addYears(4),
      Forecast_Volume__c = 1000000,
      Opportunity__c = opp01.Id
    ));
    RoyaltyCalculationHandler.isDebug = true;
    insert listToInsert;

    Test.stopTest();

    listToInsert = [
      SELECT Royalty_Forecast__c, Royalty_Rate_ASP__c, Forecast_Volume__c
      FROM Royalty_Forecast__c
      WHERE Opportunity__c = :opp01.Id
      ORDER BY Forecast_Year__c ASC
    ];

    System.assertEquals(listToInsert[0].Forecast_Volume__c * fixedRoyaltyAmount, listToInsert[0].Royalty_Forecast__c);
    System.assertEquals(listToInsert[1].Forecast_Volume__c * fixedRoyaltyAmount, listToInsert[1].Royalty_Forecast__c);
    System.assertEquals(listToInsert[2].Forecast_Volume__c * fixedRoyaltyAmount, listToInsert[2].Royalty_Forecast__c);
    System.assertEquals(listToInsert[3].Forecast_Volume__c * fixedRoyaltyAmount, listToInsert[3].Royalty_Forecast__c);
    System.assertEquals(listToInsert[4].Forecast_Volume__c * fixedRoyaltyAmount, listToInsert[4].Royalty_Forecast__c);
  }


    
}