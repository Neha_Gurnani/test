<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>2-3 cores</label>
    <protected>false</protected>
    <values>
        <field>Max__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Min__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Multiplier__c</field>
        <value xsi:type="xsd:double">1.25</value>
    </values>
</CustomMetadata>
