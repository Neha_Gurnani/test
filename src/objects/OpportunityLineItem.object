<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>AddProduct</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>ChoosePricebook</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>EditAllProduct</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Description</fullName>
    </fields>
    <fields>
        <fullName>Discount</fullName>
    </fields>
    <fields>
        <fullName>Fixed_Royalties_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Fixed Royalties Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ListPrice</fullName>
    </fields>
    <fields>
        <fullName>NumberOfInstantiations__c</fullName>
        <description>Total number of Instantiations for this product.</description>
        <externalId>false</externalId>
        <inlineHelpText>Total number of Instantiations for this product.</inlineHelpText>
        <label># of Instantiations</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OpportunityId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PricewithDiscount__c</fullName>
        <description>TotalPrice /  Quantity</description>
        <externalId>false</externalId>
        <formula>TotalPrice /  Quantity</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This is the price including the discount.</inlineHelpText>
        <label>Price with Discount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Product2Id</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductCode</fullName>
    </fields>
    <fields>
        <fullName>Product_Family__c</fullName>
        <externalId>false</externalId>
        <label>Product Family</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Quantity</fullName>
    </fields>
    <fields>
        <fullName>Royalties_Type__c</fullName>
        <externalId>false</externalId>
        <label>Royalties Type</label>
        <picklist>
            <picklistValues>
                <fullName>Percentage</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Fixed</fullName>
                <default>false</default>
            </picklistValues>
            <restrictedPicklist>true</restrictedPicklist>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Royalties__c</fullName>
        <description>This is the Royalties specific to this Opportunity Product.</description>
        <externalId>false</externalId>
        <formula>PricebookEntry.Product2.Starting_Royalty__c</formula>
        <inlineHelpText>This is the Royalties specific to this Opportunity Product.</inlineHelpText>
        <label>Royalties (%)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Royalties_number__c</fullName>
        <externalId>false</externalId>
        <label>Royalties (number)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Sales_Price1__c</fullName>
        <description>Use this Sales_Price to insert on Opportunity Product page. Calculates product list price * discount %.</description>
        <externalId>false</externalId>
        <formula>ListPrice  *  Discount</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Sales Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Sales_Price_a__c</fullName>
        <externalId>false</externalId>
        <formula>ListPrice  -  (  ListPrice  *  Discount  )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Sales Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ServiceDate</fullName>
    </fields>
    <fields>
        <fullName>Subtotal</fullName>
    </fields>
    <fields>
        <fullName>TotalPrice</fullName>
    </fields>
    <fields>
        <fullName>Total_List_Price__c</fullName>
        <externalId>false</externalId>
        <formula>ListPrice * Quantity</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total List Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UnitPrice</fullName>
    </fields>
    <fields>
        <fullName>Volume__c</fullName>
        <description>Volume (units) for this specific product.</description>
        <externalId>false</externalId>
        <inlineHelpText>Volume (units) for this specific product.</inlineHelpText>
        <label>Volume</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
</CustomObject>
