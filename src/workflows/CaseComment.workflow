<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Emil_notification_for_new_case_comment</fullName>
        <description>Emil notification for new case comment</description>
        <protected>false</protected>
        <recipients>
            <recipient>Support Engineer</recipient>
            <type>caseTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_aletr_on_new_comments</template>
    </alerts>
    <rules>
        <fullName>alert case team on new comments</fullName>
        <actions>
            <name>Emil_notification_for_new_case_comment</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>CaseComment.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>create email alert to case team on new case comments</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
