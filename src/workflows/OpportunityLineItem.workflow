<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Product_Type</fullName>
        <field>Product_Family__c</field>
        <formula>TEXT( Product2.Type__c)</formula>
        <name>Populate Product Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Royalty</fullName>
        <field>Royalties_number__c</field>
        <formula>Royalties__c</formula>
        <name>Set Royalty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Product Family</fullName>
        <actions>
            <name>Populate_Product_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Royalty</fullName>
        <actions>
            <name>Set_Royalty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Sets the royalty into a number field which can be rolled up.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
