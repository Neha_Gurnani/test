<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Payment_Number_Numeric</fullName>
        <description>Copies the numeric value of Payment_Number_Formula __c</description>
        <field>Payment_Number__c</field>
        <formula>VALUE(SUBSTITUTE(Payment_Number_formula__c,LEFT(Payment_Number_formula__c, FIND(&apos; &apos;, Payment_Number_formula__c)),&apos;&apos;))</formula>
        <name>Set Payment Number Numeric</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_By_Flow_False</fullName>
        <description>Update By Flow = False</description>
        <field>Update_By_Flow__c</field>
        <literalValue>0</literalValue>
        <name>Update By Flow = False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update By Flow %3D True</fullName>
        <actions>
            <name>Update_By_Flow_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PaymentSchedules__c.Update_By_Flow__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>PaymentSchedules__c.Payment_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>Changed Update By Flow to False after it is checked as true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Number</fullName>
        <actions>
            <name>Set_Payment_Number_Numeric</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Payment Number based on the Payment Number Formula field</description>
        <formula>ISBLANK(Payment_Number__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
