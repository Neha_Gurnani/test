<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Opportunity_Amount</fullName>
        <description>This sets the initial Total Balance Due for Scheduled Payments</description>
        <field>Total_Balance_Due__c</field>
        <formula>Amount</formula>
        <name>Copy Opportunity Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_20</fullName>
        <description>Opportunity: Set Probability 20</description>
        <field>Probability</field>
        <formula>0.20</formula>
        <name>Opportunity: Set Probability 20</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_30</fullName>
        <description>Opportunity: Set Probability 30</description>
        <field>Probability</field>
        <formula>0.30</formula>
        <name>Opportunity: Set Probability 30</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_40</fullName>
        <description>Opportunity: Set Probability 40</description>
        <field>Probability</field>
        <formula>0.40</formula>
        <name>Opportunity: Set Probability 40</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_50</fullName>
        <description>Opportunity: Set Probability 50</description>
        <field>Probability</field>
        <formula>0.50</formula>
        <name>Opportunity: Set Probability 50</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_60</fullName>
        <description>Opportunity: Set Probability 60</description>
        <field>Probability</field>
        <formula>0.60</formula>
        <name>Opportunity: Set Probability 60</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_70</fullName>
        <description>Opportunity: Set Probability 70</description>
        <field>Probability</field>
        <formula>0.70</formula>
        <name>Opportunity: Set Probability 70</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_80</fullName>
        <description>Opportunity: Set Probability 80</description>
        <field>Probability</field>
        <formula>0.80</formula>
        <name>Opportunity: Set Probability 80</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probability_90</fullName>
        <description>Opportunity: Set Probability 90</description>
        <field>Probability</field>
        <formula>0.90</formula>
        <name>Opportunity: Set Probability 90</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probabilty_10</fullName>
        <description>Opportunity: Set Probabilty 10</description>
        <field>Probability</field>
        <formula>0.10</formula>
        <name>Opportunity: Set Probabilty 10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Probabilty_100</fullName>
        <description>Opportunity: Set Probabilty 10</description>
        <field>Probability</field>
        <formula>1</formula>
        <name>Opportunity: Set Probabilty 100</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Update_Record_Type</fullName>
        <description>Opportunity: Update Record Type</description>
        <field>RecordTypeId</field>
        <lookupValue>IP_Sales</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opportunity: Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opportunity%3A Set IP Sales Record Type</fullName>
        <actions>
            <name>Opportunity_Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Production_Start__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 100</fullName>
        <actions>
            <name>Opportunity_Set_Probabilty_100</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>7</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>6</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 100</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 20</fullName>
        <actions>
            <name>Opportunity_Set_Probability_20</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3) OR (4 AND 1) OR (3 AND 2) AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 20</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 30</fullName>
        <actions>
            <name>Opportunity_Set_Probability_30</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3) OR (2 AND 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 30</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 40</fullName>
        <actions>
            <name>Opportunity_Set_Probability_40</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 3) OR (2 AND 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 40</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 50</fullName>
        <actions>
            <name>Opportunity_Set_Probability_50</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 50</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 60</fullName>
        <actions>
            <name>Opportunity_Set_Probability_60</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(4 AND 1) OR (3 AND 2) AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 60</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 70</fullName>
        <actions>
            <name>Opportunity_Set_Probability_70</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 70</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 80</fullName>
        <actions>
            <name>Opportunity_Set_Probability_80</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>6</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 80</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probability 90</fullName>
        <actions>
            <name>Opportunity_Set_Probability_90</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>6</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probability 90</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Set Probabilty 10</fullName>
        <actions>
            <name>Opportunity_Set_Probabilty_10</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>startsWith</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Funding_Stage__c</field>
            <operation>startsWith</operation>
            <value>1,2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IP Sales</value>
        </criteriaItems>
        <description>Opportunity: Set Probabilty 10</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Total Balance Due</fullName>
        <actions>
            <name>Copy_Opportunity_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>For next Scheduled Payment Amount calculations</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
